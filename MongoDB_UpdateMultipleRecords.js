const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    
      //update the existing game collection
      
      const find_by_type = {type:"sports"};

      const update_game_collection = {
        $set:{
            published_by: "EA Games"
        }
      };

      const result = await games.updateMany(find_by_type, update_game_collection);
      console.log(JSON.stringify(result));
    }
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);