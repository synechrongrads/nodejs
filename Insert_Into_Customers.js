var mySql = require('mysql2');
var connection = mySql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Pdntspa0!',
    database: 'node_db'
});

connection.connect(function(err){
    if (err) throw err;
    console.log("connection established successfully");
    var insert_query= "INSERT INTO Customer (name,ord_prod_id) VALUES ?;";
    var emp_data= [
        ['Shiraz',1],
        ['Conor',2],
        ['Mehrsa',1],
        ['Ashish',3],
        ['Messi',2],
    ];
    connection.query(insert_query,[emp_data],function(err,result){
        if(err) throw err;
        console.log(result.affectedRows + " record(s) inserted successfully");
    });
    
});


/*
Write Select query to query all the records
Write select query to query only specific columns
write select query to include all the records who's appropriate column has value starting with 'A'
write select query to include all the records who's appropriate column has value having atleast two  's'
write select query to include all the records who's appropriate column has value having values that ends with 'a'
create a table called Department and EmpLoyee with foreign key constraints properly.
Write a subquery to list all the records of employee table who are working in the same location 'Canada'
*/