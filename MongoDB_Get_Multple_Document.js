const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    // Query for the games that are of the genre: sports
    const query = {type:"sports"};
    const mygames = await games.find(query);

    await mygames.forEach(console.dir);
  }
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);