const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function count_records(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    // total count of games in the pcgames collection
      const estimate = await games.estimatedDocumentCount();
      console.log("estimated document count: " + estimate);
    
      //total number of games that are of the genre: sports

      const sports_games = {type: "Sports"}
      const sports_count = await games.countDocuments(sports_games);
      console.log("No.of Sports based Games: " + sports_count);

      const adv_games = {type: "Adventure"}
      const adv_count = await games.countDocuments(adv_games);
      console.log("No.of Adventure based Games: " + adv_count);
     
}
  finally{
  await client.close();
  }
  }
  count_records().catch(console.dir);