var mySql = require('mysql2');
var connection = mySql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Pdntspa0!',
    database: 'node_db'
});

connection.connect(function(err){
    if (err) throw err;
    console.log("connection established successfully");
    var insert_query= "INSERT INTO orders (product_name,quantity,Order_date) VALUES ?;";
    var prod_data= [
        ['Mouse',10,'2024-01-01'],
        ['Keyboard',20,'2023-01-01'],
        ['Projector',140,'2025-01-01'],
        ['Printer',50,'2023-10-01'],
        ['Scanner',60,'2023-11-01'],
        ['Copier',70,'2023-05-01'],
        ['RAM',80,'2023-05-01'],
        ['SSD',90,'2023-06-01'],
        ['Graphics Card',10,'2023-06-01'],
        ['Controller',10,'2023-07-01'],
    ];
    connection.query(insert_query,[prod_data],function(err,result){
        if(err) throw err;
        console.log(result.affectedRows + " record(s) inserted successfully");
    });
    
});
