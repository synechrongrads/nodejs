const os = require('os');

console.log('Free Memeory in OS: ' + os.freemem());
console.log('Home Directory: ' + os.homedir());
console.log('Host Name: ' + os.hostname());
console.log('Operating System Platform: ' + os.platform());
console.log('Operating System Type: ' + os.type());
console.log('System is Turned On for: ' + os.uptime() + " seconds");
console.log('Total Memory:' + os.totalmem() + " MB");
console.log('Temporary Folder Location:' + os.tmpdir());
console.log('Operating System Release:' + os.release());
console.log('Laptop Information:');
console.log(os.cpus());
console.log('Operating System Architecture:' + os.arch());
console.log('Network Interface Information:');
console.log(os.networkInterfaces());