const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    
      const get_one_record_by_title = {title: "God Of Wars"};
      const options = {upsert:true};
     
      const replaced_content = {
        title:"God Of Wars Version 4",
        director:"Cory Barlog",
        developer:{
            name:"Conor",
            email:"conor@synechron.ca",
            role:" development executive "
        }
      };

      const result = await games.replaceOne(get_one_record_by_title,replaced_content,options);
      if(result.modifiedCount ===0 && result.upsertedCount===0){
        console.log("No records were updated in the games collection");
      }
      else{
        if(result.matchedCount === 1){
            console.log(" Matched Records: " + result.matchedCount);
        }
        if(result.modifiedCount === 1){
            console.log(" Modified Records: " + result.modifiedCount);            
        }
        if(result.upsertedCount === 1) { 
            console.log(" Upserted Record with id: " + result.upsertedId); 
      }
    }
}
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);