const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    
      //create a new game entry

      const new_game = {title:"Prince of Persia",director:"Brendun McCalum",developer:{name:"George Bush",email:"bush@uscongress.com",role:"architect"},type:"adventure"};
      const result = await games.insertOne(new_game);
      console.log(`${result.insertedCount} game(s) got inserted into MongoDB Database`);

  }
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);