var http = require('http');

http.createServer(function(req, res){
    res.writeHead(200, {'content-type': 'text/plain'});
    res.end('Welcome to Node Js Server');
}).listen(8100);
console.log('Node Js Server listening on http://localhost:8100');