const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    
      const get_records_by_type = {type: "sports"};

      const result = await games.deleteMany(get_records_by_type);

    console.log("No.of games deleted: " + result.deletedCount);
      
}
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);