const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    
      const get_one_record_by_title = {title: "God Of Wars Version 4"};

      const result = await games.deleteOne(get_one_record_by_title);

      if(result.deletedCount === 1){
        console.log("successfully deleted one game from the game collection");
      }
      else{
        console.log("failed to delete one game from the game collection as there is no such game in the collection");
      }
      
}
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);