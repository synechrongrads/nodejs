const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    
      //create a new game entry

      const new_games = [
        {
            title:"Hockey",
            director:"Vladmir Putin",
            developer:
                {
                    name:"Johnty Rhodes",
                    email:"rhodes@hockeyfederation.com",
                    role:"architect"
                },
            type:"sports"
        },
        {
            title:"Javelin",
            director:"Sadam Hussain",
            developer:
                {
                    name:"Tom Cruise",
                    email:"tomcruise@missionimpossible.com",
                    role:"developer"
                },
            type:"sports"
        }

    ];
      const options = { ordered: true };
      const result = await games.insertMany(new_games,options);
      console.log(`game(s) got inserted into MongoDB Database`);

  }
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);