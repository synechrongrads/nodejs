const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
try{
  await client.connect();
  const database = client.db("PS4Games");
  const games = database.collection("pcgames");

  //Querying for a specific document in the database

  const query = {title:"God Of Wars"};
  const mygame = await games.findOne(query);

  console.log(mygame);
}
finally{
await client.close();
}
}
run().catch(console.dir);