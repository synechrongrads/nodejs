const { MongoClient } = require('mongodb');
const url = "mongodb://127.0.0.1:27017/";
const client = new MongoClient(url,{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

async function run(){
    try{
      await client.connect();
      const database = client.db("PS4Games");
      const games = database.collection("pcgames");
    
      //update the existing game collection

      // Step 1: Find the document to be updated

      const game_to_be_found = {title:"Hockey"};

      // Step 2: Create a new document if not exist and if it already exists,it will update based on the query below
      const options = {upsert:true};

      // Create the new document or update the existing document
      const update_game = {
        $set:{
            age_category:"Adults",
        }
      };

      const result = await games.updateOne(game_to_be_found,update_game,options);
      console.log("Updated the game collection");
    }
  finally{
  await client.close();
  }
  }
  run().catch(console.dir);